package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test 
    public void checkEcho()
    {
        App ap = new App();
        ap.echo(5);
        assertEquals("This is a message telling you the test is about checking the echo method is suppose to return the input value.", 5, ap.echo(5), 0);
    }

    @Test
    public void checkOneMore()
    {
        App ap = new App();
        ap.oneMore(5);
        assertEquals("This is a test to see if the oneMore() method returns a value one more than the input", 6, ap.oneMore(5), 0);
    }
}
